package com.module.test.web;

import com.module.test.entity.Test;
import com.module.test.repository.TestRepositoryTest;
import com.module.test.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import javax.annotation.Resource;
import java.util.List;

@RequestMapping("test")
@Controller
public class TestController {

    @Resource
    private TestRepositoryTest testRepositoryTest;

    @Autowired
    private TestService testService;

    @RequestMapping("/index")
    public ModelAndView index(){
        List<Test> list = testRepositoryTest.test();
        return new ModelAndView("index")
                .addObject("list", list);
    }

    @RequestMapping("/mybatis/plus")
    public ModelAndView index2(){
        List<Test> userList = testService.list();
        return new ModelAndView("index")
                .addObject("list", userList);
    }
}
