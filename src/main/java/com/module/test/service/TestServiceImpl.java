package com.module.test.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.module.test.mapper.TestMapper;
import com.module.test.entity.Test;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements TestService {
}
