package com.module.test.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.module.test.entity.Test;

public interface TestService extends IService<Test> {
}
