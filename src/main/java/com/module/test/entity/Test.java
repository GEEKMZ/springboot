package com.module.test.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="test")
@Data
public class Test {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Integer age;
}
