package com.module.test.repository;

import com.module.test.entity.Test;
import org.springframework.data.jpa.repository.JpaRepository;
public interface TestRepository extends JpaRepository<Test, Long> {

}
