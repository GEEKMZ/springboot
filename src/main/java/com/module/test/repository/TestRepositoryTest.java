package com.module.test.repository;

import com.module.test.entity.Test;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:  jpa 写法 一般不使用
 *
 * @ClassName: TestRepositoryTest
 * @author czw
 * @since JDK1.7
 * @date 2020/4/6
 * @version V1.0
 */
@Repository
public class TestRepositoryTest {

    @Resource
    private TestRepository userRepository;

    public List<Test> test(){
        Test test = new Test();
        test.setAge(344);
        test.setName("铁憨憨");
        userRepository.save(test);
        return userRepository.findAll();
    }
}
